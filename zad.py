import math
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.animation as animation
import matplotlib.lines as lines

# parameters
# l = 10
# k = 10

def animate(i, dt):
    line.set_xdata(ux1[:i])
    line.set_ydata(uy1[:i])
    line2.set_xdata(ux2[:i])
    line2.set_ydata(uy2[:i])
    return line,line2

def calculate():
    m = np.array( [[1,0,0,0],[0,1,0,0], [0,0,1,0], [0,0,0,1]])
    M = np.linalg.inv(m)
    C = np.zeros(4)
    steps = 100
    f = np.array( [[0],[-9.81 * 1],[0],[-9.81 * 1]])
    t = [0 for x in range(steps)]
    x1 = [0 for x in range(steps)]
    y1 = [0 for x in range(steps)]
    x2 = [0 for x in range(steps)]
    y2 = [0 for x in range(steps)]
    ux1 = [0 for x in range(steps)]
    ux1 = [0 for x in range(steps)]
    uy1 = [0 for x in range(steps)]
    ux2 = [0 for x in range(steps)]
    uy2 = [0 for x in range(steps)]
    vx1 = [0 for x in range(steps)]
    vy1 = [0 for x in range(steps)]
    vx2 = [0 for x in range(steps)]
    vy2 = [0 for x in range(steps)]
    ax1 = [0 for x in range(steps)]
    ay1 = [0 for x in range(steps)]
    ax2 = [0 for x in range(steps)]
    ay2 = [0 for x in range(steps)]
    a1 = [0 for x in range(steps)]
    a2 = [0 for x in range(steps)]
    a3 = [0 for x in range(steps)]
    a4 = [0 for x in range(steps)]

    ux2[0] = -10
    vy1[0] = 50
    vx2[0] = -30
    vy2[0] = 30
    a3[0] = -10

    ux1_previous = ux1[0]
    ux2_previous = ux2[0]
    uy1_previous = uy1[0]
    uy2_previous = uy2[0]
    vx1_previous = vx1[0]
    vx2_previous = vx2[0]
    vy1_previous = vy1[0]
    vy2_previous = vy2[0]

    s = 1
    while s < steps:
        u = np.array([[ux1_previous],[uy1_previous],[ux2_previous],[uy2_previous]])
        v = np.array([[vx1_previous],[vy1_previous],[vx2_previous],[vy2_previous]])

        dx = ux1_previous - ux2_previous
        dy = uy1_previous - uy2_previous
        if dy == 0:
            cosinus = math.cos(0)
            sinus = math.sin(0)
        else:
            cosinus = math.cos(math.atan(dx/dy))
            sinus = math.sin(math.atan(dx/dy))

        tr = np.array([[cosinus*cosinus,cosinus*sinus,-cosinus*cosinus,-cosinus*sinus],[cosinus*sinus,sinus*sinus,-cosinus*sinus,-sinus*sinus],[-cosinus*cosinus,-cosinus*sinus,cosinus*cosinus,cosinus*sinus],[-cosinus*sinus,-sinus*sinus,cosinus*sinus,sinus*sinus]])

        v_1 = M.dot((f - np.dot(10*tr, u)) * 0.001) + v
        Vdt = v * 0.001
        u_1 = Vdt + u

        t[s] = t[s-1] + 0.001

        vx1[s]=v_1[0,0]
        vx2[s]=v_1[2,0]

        vy1[s]=v_1[1,0]
        vy2[s]=v_1[3,0]

        ux1[s]=u_1[0,0]
        ux2[s]=u_1[2,0]

        uy1[s]=u_1[1,0]
        uy2[s]=u_1[3,0]

        a1[s]=u_1[0,0]
        a2[s]=u_1[1,0]
        a3[s]=u_1[2,0]
        a4[s]=u_1[3,0]

        ux1_previous = ux1[s]
        ux2_previous = ux2[s]
        uy1_previous = uy1[s]
        uy2_previous = uy2[s]
        vx1_previous = vx1[s]
        vx2_previous = vx2[s]
        vy1_previous = vy1[s]
        vy2_previous = vy2[s]

        print('step', s, ux1, uy1, ux2, uy2)

        if uy1[s] < 0 or uy2[s] < 0:
            break
        s = s + 1

    fig = plt.figure()
    ax = fig.add_subplot()
    line, = ax.plot([],[], '-')
    line2, = ax.plot([],[],'-')
    ax.set_xlim(np.min(ux1 or ux2)-50, np.max(ux1 or ux2)+50)
    ax.set_ylim(np.min(uy1 or uy2)-10, np.max(uy1 or uy2)+50)

    ani = animation.FuncAnimation(fig, animate, frames=len(ux1), fargs=(0.5,), interval=dt, blit=True)
    plt.show()

calculate()
